Quickstart Development
=======================

This guide gives you a fast introduction into the sila_python
development workflow (:doc:`development/0_sila_development_workflow`).

Clone the git repository
-------------------------

.. code-block:: console

  git clone https://gitlab.com/SiLA2/sila_python

  cd sila_python

  python3 sila2installer.py  # this installs a python virtualenv and all dependencies

  # choose 'd'- option to install the development environment

For other installtion options, please refer to SiLA 2 :doc:`installation/0_installation`

For further development steps, please see:

:doc:`development/0_sila_development_workflow`

and

:doc:`development/1_contributing_sila_python`
