Codegenerator Demo and Tutorial
================================

To run the demo, make sure, you are in a folder containing the
hello\_sila\_project folder and run:

.. code:: bash

        source [venv]/bin/activate    # this activates the virtual python environment

        cd [folder containing HelloSiLA2_project]

        python3 silacodegenerator.py -b HelloSiLA2_project

This will create a directory, called "HelloSiLA2\_build". It will
contain the following files:

-  HelloSiLA\_server.py - the main "SiLA2 Server/Device file": run this
   to start the SiLA2 Server [python3 HelloSiLA2\_server.py]
-  HelloSiLA\_testclient.py - the "SiLA2 client/orchestrator": run this
   to send commands to the SiLA Server [python3
   HelloSiLA2\_testclient.py]

-  GreetingProvider\_pb2.py - the underlying Protobuf / gRPC bindings
-  GreetingProvider\_pb2\_grpc.py - the underlying gRPC bindings

-  GreetingProvider\_simulation.py - this code can be used for the
   simulation mode
-  GreetingProvider\_real.py - this code can be used for writing
   real-life / hardware connections (in case the service is connected to
   hardware)

To actually see data transfer between the SiLA2 Server and the SiLA2
client, please uncomment/change the following lines:

::

      in HelloSiLA2_simulation.py / sayHello() add:
         return pb2.SayHelloResponses(Greeting="Hello world !")

      in HelloSiLA2_testclient.py / run() change:
         result = self.HelloSiLA_serv.SayHello(HelloSiLA_pb2.SayHelloParameters(Name="SiLA2"))
         logging.info(result) # this will display the answer from the server

Future Plans
------------

-  library of standard features, that are already connected
   (SiLAService, ServiceControl, Simulation, ...)
-  django app to control the device remotely through the web
-  QT5 client / server apps
