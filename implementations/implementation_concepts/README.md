
# Implementation Concepts - The experimentation Lab

If you have ideas for new implementation concepts or just want 
to try out something new and show it to the others, this is the place
to put your project. 

Feel free to experiment - this is our experimentation lab !


## Current experiments / concepts / proof-of-concepts

  1. gRPC-web SiLA browser
      - a SiLA web client, that does not require a webserver, but completely runs in your webbrowser, 
        using the new gRPC-web technology
  
  2. late_bindng_grpc_reflections
     - a SiLA server-agnostic client, that uses grpc_reflections to query a SiLA server 


