
# gRPC-web howto

This is a short tutorial how to create and build a gRPC-web application:

1. build [protobuf compiler protoc](#building_protoc)
1. install [gRPC-web plugins]()
1. generate and [compile protoc file](#compiling_proto_file)
1. install and run [Envoy proxy](#envoy_installation)
1. make a [gRPC test server](#gRPC_test_server) (s. todo_server in this directory)
1. add [javascript gRPC calls] to your html code
1. [calling the webpage]

[example](https://github.com/grpc/grpc-web/tree/master/net/grpc/gateway/examples/echo)


[ngix configuration](https://github.com/grpc/grpc-web/blob/master/net/grpc/gateway/examples/echo/nginx.conf)


USE THOSE COMMANDS AT YOUR OWN RISK. 
I SHALL NOT BE RESPONSIBLE FOR ANYTHING.
ABSOLUTELY NO WARRANTY.

## Compiling proto file

In this example [CommonJS](https://requirejs.org/docs/commonjs.html) is used as a compiler backend:

```
mkdir generated
protoc -I="." myfile.proto \
  --js_out=import_style=commonjs:generated \
  --grpc-web_out=import_style=commonjs,mode=grpcwebtext:generated
```
 
## Building/Installing protoc

For Linux, Mac and MinGW:

Make sure, that [gcc > v6.1.1 is installed](#gcc-6_installation)

```
git clone --recurse-submodules https://github.com/google/protobuf.git
cd protobuf
# check if version >v3.5.1  git checkout v3.5.1
git tag
./autogen.sh
./configure
make
make check
sudo make install
```

If you are comfortable with C++ compilation and autotools, you can specify a
``--prefix`` for Protobuf and use ``-I`` in ``CXXFLAGS``, ``-L`` in
``LDFLAGS``, ``LD_LIBRARY_PATH``, and ``PATH`` to reference it. The
environment variables will be used when building grpc-java.

Protobuf installs to ``/usr/local`` by default.

In case the third-party modules have not been cloned:

    git submodule update --init --recursive

## gRPC plugin Installation

npm i grpc-web
git clone https://github.com/grpc/grpc-web
cd grpc-web && sudo make install-plugin


## Envoy Installation

s. [Envoy](https://envoyproxy.io/)

and [Envoy Documentation](https://www.envoyproxy.io/docs/envoy/latest/)

### Envoy docker

```
sudo docker pull envoyproxy/envoy:latest
# running
sudo docker run --rm -d -p 10000:10000 envoyproxy/envoy:latest
# testing
curl -v localhost:10000
```

[source](https://www.envoyproxy.io/docs/envoy/latest/start/start#quick-start-to-run-simple-example)

or

$ git clone https://github.com/grpc/grpc-web
$ cd grpc-web
$ docker-compose up node-server envoy commonjs-client


## javascript gRPC example

```
const {GetTodoRequest} = require('./todos_pb.js');
const {TodoServiceClient} = require('./todos_grpc_web_pb.js');

const todoService = new proto.todos.TodoServiceClient('http://localhost:8080');
const todoId = 1234;

var getTodoRequest = new proto.todos.GetTodoRequest();
getTodoRequest.setId(todoId);

var metadata = {};
var getTodo = todoService.getTodoById(getTodoRequest, metadata, (err, response) => {
  if (err) {
    console.log(err);
  } else {
    const todo = response.todo();
    if (todo == null) {
      console.log(`A TODO with the ID ${todoId} wasn't found`);
    } else {
      console.log(`Fetched TODO with ID ${todoId}: ${todo.content()}`);
    }
  }
});
```

## gRPC test server

compling the python stubs

    python -m grpc_tools.protoc -Iprotos --python_out=. --grpc_python_out=. protos/todos.proto


## Calling the webpage

Open a browser tab, and go to:

    http://localhost:8081/echotest.html


## gcc-6 Installation 

These commands are based on a askubuntu answer http://askubuntu.com/a/581497
To install gcc-6 (gcc-6.1.1), I had to do more stuff as shown below.

If you are still reading let's carry on with the code.

sudo apt-get update && \
sudo apt-get install build-essential software-properties-common -y && \
sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y && \
sudo apt-get update && \
sudo apt-get install gcc-snapshot -y && \
sudo apt-get update && \
sudo apt-get install gcc-6 g++-6 -y && \
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 60 --slave /usr/bin/g++ g++ /usr/bin/g++-6 && \
sudo apt-get install gcc-4.8 g++-4.8 -y && \
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 60 --slave /usr/bin/g++ g++ /usr/bin/g++-4.8;

When completed, you must change to the gcc you want to work with by default. Type in your terminal:
sudo update-alternatives --config gcc

To verify if it worked. Just type in your terminal
gcc -v

If everything went fine you should see gcc 6.1.1  by the time I am writing this gist

Happy coding!

[source](https://gist.github.com/application2000/73fd6f4bf1be6600a2cf9f56315a2d91)
