"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Observable Property Provider*

:details: ObservablePropertyProvider:
    Feature definition that defines an observable property

:file:    ObservablePropertyProvider_real.py
:authors: Timm Severin, Lukas Bromig

:date: (creation)          2020-03-12T16:18:13.914985
:date: (last modification) 2020-03-12T16:18:13.914985

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "1.0"

# import general packages
import logging
import time         # used for observables
import uuid         # used for observables
import grpc         # used for type hinting only

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2

# import gRPC modules for this feature
from .gRPC import ObservablePropertyProvider_pb2 as ObservablePropertyProvider_pb2
# from .gRPC import ObservablePropertyProvider_pb2_grpc as ObservablePropertyProvider_pb2_grpc

# import default arguments
from .ObservablePropertyProvider_default_arguments import default_dict


# noinspection PyPep8Naming,PyUnusedLocal
class ObservablePropertyProviderReal:
    """
    Implementation of the *Observable Property Provider* in *Real* mode
        This service can be used to test observable properties
    """

    def __init__(self):
        """Class initialiser"""

        logging.debug('Started server in mode: {mode}'.format(mode='Real'))

    def _get_command_state(self, command_uuid: str) -> silaFW_pb2.ExecutionInfo:
        """
        Method to fill an ExecutionInfo message from the SiLA server for observable commands

        :param command_uuid: The uuid of the command for which to return the current state

        :return: An execution info object with the current command state
        """

        #: Enumeration of silaFW_pb2.ExecutionInfo.CommandStatus
        command_status = silaFW_pb2.ExecutionInfo.CommandStatus.waiting
        #: Real silaFW_pb2.Real(0...1)
        command_progress = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_estimated_remaining = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_lifetime_of_execution = None

        # TODO: check the state of the command with the given uuid and return the correct information

        # just return a default in this example
        return silaFW_pb2.ExecutionInfo(
            commandStatus=command_status,
            progressInfo=(
                command_progress if command_progress is not None else None
            ),
            estimatedRemainingTime=(
                command_estimated_remaining if command_estimated_remaining is not None else None
            ),
            updatedLifetimeOfExecution=(
                command_lifetime_of_execution if command_lifetime_of_execution is not None else None
            )
        )

    def SetProperty(self, request, context: grpc.ServicerContext) \
            -> ObservablePropertyProvider_pb2.SetProperty_Responses:
        """
        Executes the unobservable command "Set property"
            Allows to set the value of the property to change its value.
            This can be used to test if changes in the value are accepted by the property when running asynchronously.TODO: Implement this feature in the simulation or real mode.
    
        :param request: gRPC request containing the parameters passed:
            request.Value (Value): New parameter value
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.EmptyResponse (Empty Response): An empty response data type used if no response is required.
        """
    
        # initialise the return value
        return_value = None
    
        # TODO:
        #   Add implementation of Real for command SetProperty here and write the resulting response
        #   in return_value
    
        # fallback to default
        if return_value is None:
            return_value = ObservablePropertyProvider_pb2.SetProperty_Responses(
                **default_dict['SetProperty_Responses']
            )
    
        return return_value
    

    def Subscribe_Value(self, request, context: grpc.ServicerContext) \
            -> ObservablePropertyProvider_pb2.Subscribe_Value_Responses:
        """
        Requests the observable property Value
            This property returns random integer numbers at the startup of the server while being observed.
            Once a value is set via the :meth:`SetProperty` this value will be returned.
            TODO: Implement this feature in the simulation or real mode.
                    
    
        :param request: An empty gRPC request object (properties have no parameters)
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A response object with the following fields:
            request.Value (Value): 
            This property returns random integer numbers at the startup of the server while being observed.
            Once a value is set via the :meth:`SetProperty` this value will be returned.
            TODO: Implement this feature in the simulation or real mode.
        """
    
        # initialise the return value
        return_value: ObservablePropertyProvider_pb2.Subscribe_Value_Responses = None
    
        # we could use a timeout here if we wanted
        while True:
            # TODO:
            #   Add implementation of Real for property Value here and write the resulting
            #   response in return_value
    
            # create the default value
            if return_value is None:
                return_value = ObservablePropertyProvider_pb2.Subscribe_Value_Responses(
                    **default_dict['Subscribe_Value_Responses']
                )
    
    
            yield return_value
    
